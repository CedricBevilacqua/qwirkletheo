﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QwirkleGame;

namespace QwirkleGameTests
{
    [TestClass]
    public class JoueurTests
    {
        [TestMethod]
        public void AddNameTest()
        {
            Joueur Louis = new Joueur();
            Louis.AddName("Louis");
            Assert.AreEqual("Louis", Louis.GetName());
        }

        [TestMethod]
        public void AddAgeTest()
        {
            Joueur Louis = new Joueur();

            Louis.AddAge(20);
            Assert.AreEqual(20, Louis.GetAge());

            Louis.AddAge(-5);
            Assert.AreEqual(20, Louis.GetAge());

            Louis.AddAge(0);
            Assert.AreEqual(20, Louis.GetAge());
        }

        [TestMethod]
        public void AddPointsTest()
        {
            Joueur Louis = new Joueur();

            Louis.AddPoint(0);
            Louis.AddPoint(30);
            Louis.AddPoint(54);
            Louis.AddPoint(0);
            Louis.AddPoint(-4);

            Assert.AreEqual(84,Louis.GetPoint());
        }

        [TestMethod]
        public void AddToChangeTest()
        {
            Joueur.GetPlayerTab(1).AddToChange(1);
            Assert.AreEqual(true,Joueur.GetPlayerTab(1).GetToChange()[1]);
        }

        [TestMethod]
        public void RemoveToChangeTest()
        {
            Joueur.GetPlayerTab(1).RemoveToChange(1);
            Assert.AreEqual(false, Joueur.GetPlayerTab(1).GetToChange()[1]);
        }

        [TestMethod]
        public void ResetToChangeTest()
        {
            Joueur.GetPlayerTab(1).AddToChange(0);
            Joueur.GetPlayerTab(1).AddToChange(1);
            Joueur.GetPlayerTab(1).AddToChange(5);
            Joueur.GetPlayerTab(1).ResetToChange();
            Assert.AreEqual(false, Joueur.GetPlayerTab(1).GetToChange()[0]);
            Assert.AreEqual(false, Joueur.GetPlayerTab(1).GetToChange()[1]);
            Assert.AreEqual(false, Joueur.GetPlayerTab(1).GetToChange()[5]);
        }

        [TestMethod]
        public void ChangeTest()
        {
        }

        [TestMethod]
        public void VerifAvaiablePossibilitiesTest()
        {
        }

        [TestMethod]
        public void AddNbJoueurTest()
        {
            Joueur.AddNbJoueur();
            Joueur.AddNbJoueur();
            Joueur.AddNbJoueur();
            Joueur.AddNbJoueur();
            Joueur.AddNbJoueur();

            Assert.AreEqual(4,Joueur.GetNbJoueur());
        }

        [TestMethod]
        public void RemoveNbJoueurTest()
        {
            for (int Boucle = 0; Boucle < 4; Boucle++)
            {
                Joueur.RemoveNbJoueur();
            }
            Joueur.RemoveNbJoueur();
            Joueur.AddNbJoueur();
            Joueur.AddNbJoueur();
            Joueur.AddNbJoueur();
            Joueur.RemoveNbJoueur();
            Joueur.RemoveNbJoueur();

            Assert.AreEqual(1,Joueur.GetNbJoueur());
        }

        [TestMethod]
        public void DistributionPionsTest()
        {
            while (Joueur.GetNbJoueur() < 4)
            {
                Joueur.AddNbJoueur();
            }
            Joueur.DistributionPion();
        }

        [TestMethod]
        public void SeeMainTest()
        {
            if (Joueur.GetPlayerTab(0).SeeMain(0).GetCouleur() != "Yellow") { }
            else if (Joueur.GetPlayerTab(0).SeeMain(0).GetCouleur() != "Blue") { }
            else if (Joueur.GetPlayerTab(0).SeeMain(0).GetCouleur() != "Pink") { }
            else if (Joueur.GetPlayerTab(0).SeeMain(0).GetCouleur() != "Green") { }
            else if (Joueur.GetPlayerTab(0).SeeMain(0).GetCouleur() != "Orange") { }
            else if (Joueur.GetPlayerTab(0).SeeMain(0).GetCouleur() != "Red") { }
            else
            {
                throw new Exception();
            }
        }

        [TestMethod]
        public void SeeActualPlayerTest()
        {
            Assert.AreEqual(1, Joueur.SeeActualPlayer());
        }

        [TestMethod]
        public void NextPlayerTest()
        {
            Assert.AreEqual(1, Joueur.SeeActualPlayer());
            Joueur.NextPlayer();
            Assert.AreEqual(2, Joueur.SeeActualPlayer());
            Joueur.NextPlayer();
            Assert.AreEqual(3, Joueur.SeeActualPlayer());
            Joueur.NextPlayer();
            Assert.AreEqual(4, Joueur.SeeActualPlayer());
            Joueur.NextPlayer();
            Assert.AreEqual(1, Joueur.SeeActualPlayer());
        }

        [TestMethod]
        public void FirstToStartTest()
        {
        }

        [TestMethod]
        public void SeeWinnerTest()
        {
            while(Joueur.GetNbJoueur()<4)
            {
                Joueur.AddNbJoueur();
            }

            Joueur.GetPlayerTab(0).AddPoint(100);
            Joueur.GetPlayerTab(1).AddPoint(200);
            Joueur.GetPlayerTab(2).AddPoint(300);
            Joueur.GetPlayerTab(3).AddPoint(400);

            Assert.AreEqual(4,Joueur.SeeWinner());

            Joueur.GetPlayerTab(2).AddPoint(300);
            Assert.AreEqual(3, Joueur.SeeWinner());
        }

        [TestMethod]
        public void GetPlayerTabTest()
        {
            Joueur.GetPlayerTab(0).AddName("A");
            Joueur.GetPlayerTab(1).AddName("B");
            Joueur.GetPlayerTab(2).AddName("C");
            Joueur.GetPlayerTab(3).AddName("D");

            Assert.AreEqual("A",Joueur.GetPlayerTab(0).GetName());
            Assert.AreEqual("B", Joueur.GetPlayerTab(1).GetName());
            Assert.AreEqual("C", Joueur.GetPlayerTab(2).GetName());
            Assert.AreEqual("D", Joueur.GetPlayerTab(3).GetName());
        }
    }
}
