﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using QwirkleGame;
using System;

namespace QwirkleGameTests
{
    [TestClass]
    public class PlateauTests
    {
        [TestMethod]
        public void CountPointTest()
        {
        }

        [TestMethod]
        public void AddPieceTest()
        {
            Plateau.AddPieceToAdd(Joueur.GetPlayerTab(0).SeeMain(1), 3, 13);
            try
            {
                Plateau.AddPiece();
            }
            catch
            {
                throw new Exception();
            }
            if (Plateau.GetCaseInformation()[3, 13] == 0)
            {
                throw new Exception();
            }
            try
            {
                Tuile Test = Plateau.GetPiecesInformation(3, 13);
            }
            catch
            {
                throw new Exception();
            }
            Plateau.ResetPiecesToAdd();
        }

        [TestMethod]
        public void VerifPlacementTest()
        {
        }

        [TestMethod]
        public void AddPiecesToAddTest()
        {
            Plateau.ResetPiecesToAdd();
            Plateau.AddPieceToAdd(Joueur.GetPlayerTab(0).SeeMain(0), 10, 5);
        }

        [TestMethod]
        public void GetToAddPieceInformationTest()
        {
            try
            {
                Tuile RecuperatedPiece = Plateau.GetToAddPiecesInformation(0);
                Assert.IsNull(Plateau.GetToAddPiecesInformation(1));
            }
            catch
            {
                throw new Exception();
            }
        }

        [TestMethod]
        public void GetPieceInformationTest()
        {
            Plateau.AddPiece();
            Tuile Test = Plateau.GetPiecesInformation(10,5);
        }

        [TestMethod]
        public void ResetPiecesToAddTest()
        {
            Plateau.ResetPiecesToAdd();

            Assert.AreEqual(0, Plateau.GetPiecesToAdd());
            Assert.AreEqual(0, Plateau.GetNbPiecesToAdd());

            for (int ParcoursCol = 0; ParcoursCol < 2; ParcoursCol++)
            {
                for (int ParcoursLn = 0; ParcoursLn < 6; ParcoursLn++)
                {
                    if (Plateau.GetPosToAdd()[ParcoursLn, ParcoursCol] != 0)
                    {
                        throw new Exception();
                    }
                }
            }
        }

        [TestMethod]
        public void GetCaseInformationTest()
        {
            int ToTestValue;
            try
            {
                for (int ParcoursCol = 0; ParcoursCol < 20; ParcoursCol++)
                {
                    for (int ParcoursLn = 0; ParcoursLn < 20; ParcoursLn++)
                    {
                        ToTestValue = Plateau.GetCaseInformation()[ParcoursLn, ParcoursCol];
                    }
                }
            }
            catch
            {
                throw new Exception();
            }
        }

        [TestMethod]
        public void GetToAddCaseInformationTest()
        {
            int ToTestValue;
            try
            {
                for (int ParcoursCol = 0; ParcoursCol < 2; ParcoursCol++)
                {
                    for (int ParcoursLn = 0; ParcoursLn < 6; ParcoursLn++)
                    {
                        ToTestValue = Plateau.GetToAddCaseInformation()[ParcoursLn, ParcoursCol];
                    }
                }
            }
            catch
            {
                throw new Exception();
            }
        }
    }
}
