﻿using System;
using System.Collections.Generic;

namespace QwirkleGame
{
    public class Joueur
    {
        // Attributs
        private string Name;
        private int Age;
        private List<Tuile> Main = new List<Tuile>();
        private int Points = 0;
        private static int NbJoueurs = 2;
        private Boolean CanValidateTurn; //Les conditions sont remplies pour passer au tour suivant
        private static int ActualPlayer = 1; //Joueur actuellement en jeu
        private static Joueur[] TabPlayers = new Joueur[4];
        private static bool[] MainToChange = new bool[6];

        // Constructeur
        public Joueur()
        {

        }

        static Joueur()
        {
            for (int Boucle = 0; Boucle < 4; Boucle++)
            {
                TabPlayers[Boucle] = new Joueur();
            }
        }

        // Méthodes
        public void AddName(string Name)
        {
            this.Name = Name;
        }

        public void AddAge(int Age)
        {
            if (Age > 0)
            {
                this.Age = Age;
            }
        }

        public void AddPoint(int PointsToAdd)
        {
            if (PointsToAdd > 0)
            {
                Points = Points + PointsToAdd;
            }
        }

        public void Change()
        {
            for(int NbMainCompteur = 0; NbMainCompteur < 6; NbMainCompteur++)
            {
                if(MainToChange[NbMainCompteur] == true)
                {
                    Main.RemoveAt(NbMainCompteur);
                    List<Tuile> TuileToIncrust = new List<Tuile>();
                    TuileToIncrust.Add(Pioche.RandomPiece());
                    Main.InsertRange(NbMainCompteur, TuileToIncrust);
                }
            }
        }

        public void AddToChange(int NbMain)
        {
            MainToChange[NbMain] = true;
        }

        public void RemoveToChange(int NbMain)
        {
            MainToChange[NbMain] = false;
        }

        public void ResetToChange()
        {
            for (int CompteurMain = 0; CompteurMain < 6; CompteurMain++)
            {
                MainToChange[CompteurMain] = false;
            }
        }

        public Tuile SeeMain(int IndexToSee)
        {
            return this.Main[IndexToSee];
        }

        public Boolean VerifAvailablePossibilities()
        {
            throw new NotImplementedException();
        }

        public static void AddNbJoueur()
        {
            if (NbJoueurs < 4)
            {
                NbJoueurs++;
            }
        }

        public static void RemoveNbJoueur()
        {
            if (NbJoueurs > 0)
            {
                NbJoueurs--;
            }
        }

        public static void DistributionPion()
        {
            for (int JoueurTirage = 0; JoueurTirage < NbJoueurs; JoueurTirage++)
            {
                for (int PiecesTirees = 0; PiecesTirees < 6; PiecesTirees++)
                {
                    TabPlayers[JoueurTirage].Main.Add(Pioche.RandomPiece());
                }
            }
        }

        public static int FirstToStart()
        {
            int[] CommonCar = new int[NbJoueurs];

            for (int BouclePlayer = 0; BouclePlayer < NbJoueurs; BouclePlayer++)
            {
                int[] TriFormes = new int[6];
                int[] TriCouleurs = new int[6];
                //Classement des caractéristiques
                for (int NbMainCompteur = 0; NbMainCompteur < 6; NbMainCompteur++)
                {
                    Tuile MainTuile = TabPlayers[BouclePlayer].SeeMain(NbMainCompteur);
                    //Formes communes : Star, Cross, Lozenge, Flower, Circle, Square
                    if (MainTuile.GetForme() == "Star")
                    {
                        TriFormes[0]++;
                    }
                    else if (MainTuile.GetForme() == "Cross")
                    {
                        TriFormes[1]++;
                    }
                    else if (MainTuile.GetForme() == "Lozenge")
                    {
                        TriFormes[2]++;
                    }
                    else if (MainTuile.GetForme() == "Flower")
                    {
                        TriFormes[3]++;
                    }
                    else if (MainTuile.GetForme() == "Circle")
                    {
                        TriFormes[4]++;
                    }
                    else if (MainTuile.GetForme() == "Square")
                    {
                        TriFormes[5]++;
                    }
                    //Couleurs communes : Yellow, Blue, Pink, Green, Orange, Red
                    if (MainTuile.GetCouleur() == "Yellow")
                    {
                        TriCouleurs[0]++;
                    }
                    else if (MainTuile.GetCouleur() == "Blue")
                    {
                        TriCouleurs[1]++;
                    }
                    else if (MainTuile.GetCouleur() == "Pink")
                    {
                        TriCouleurs[2]++;
                    }
                    else if (MainTuile.GetCouleur() == "Green")
                    {
                        TriCouleurs[3]++;
                    }
                    else if (MainTuile.GetCouleur() == "Orange")
                    {
                        TriCouleurs[4]++;
                    }
                    else if (MainTuile.GetCouleur() == "Red")
                    {
                        TriCouleurs[5]++;
                    }
                }
                //Calcul du plus grand nombre de correspondances
                for (int ParcoursFormes = 0; ParcoursFormes < 6; ParcoursFormes++)
                {
                    if (TriFormes[ParcoursFormes] > CommonCar[BouclePlayer])
                    {
                        CommonCar[BouclePlayer] = TriFormes[ParcoursFormes];
                    }
                }
                for (int ParcoursCouleurs = 0; ParcoursCouleurs < 6; ParcoursCouleurs++)
                {
                    if (TriFormes[ParcoursCouleurs] > CommonCar[BouclePlayer])
                    {
                        CommonCar[BouclePlayer] = TriCouleurs[ParcoursCouleurs];
                    }
                }
            }
            //Calcul du joueur au plus grand nombre de caractéristiques communes
            int BestCommonCar = 0;
            int PlayerWhoStart = 0;
            for (int BoucleJoueur = 0; BoucleJoueur < NbJoueurs; BoucleJoueur++)
            {
                if (CommonCar[BoucleJoueur] > BestCommonCar)
                {
                    BestCommonCar = CommonCar[BoucleJoueur];
                    PlayerWhoStart = BoucleJoueur;
                }
            }
            //Vérification de l'unicité du plus grand nombre de caractéristiques communes
            int NbIdenticCommons = 0;
            for (int BoucleJoueur = 0; BoucleJoueur < NbJoueurs; BoucleJoueur++)
            {
                if (CommonCar[BoucleJoueur] == BestCommonCar)
                {
                    NbIdenticCommons++;
                }
            }
            //Choix en fonction de l'âge
            if (NbIdenticCommons > 1)
            {
                int Older = 0;
                int OlderPlayer = 0;
                for (int BoucleJoueur = 0; BoucleJoueur < NbJoueurs; BoucleJoueur++)
                {
                    if (TabPlayers[BoucleJoueur].GetAge() > Older)
                    {
                        Older = TabPlayers[BoucleJoueur].GetAge();
                        OlderPlayer = BoucleJoueur;
                    }
                }
                return OlderPlayer;
            }
            else
            {
                return PlayerWhoStart;
            }
        }

        public static int SeeActualPlayer()
        {
            return ActualPlayer;
        }

        public static void NextPlayer()
        {
            ActualPlayer++;
            if (ActualPlayer > NbJoueurs)
            {
                ActualPlayer = 1;
            }
        }

        public static int SeeWinner()
        {
            int NbPlayerWinner = 0;
            int BestScore = TabPlayers[0].Points;
            for (int Boucle = 1; Boucle < NbJoueurs; Boucle++)
            {
                if (TabPlayers[Boucle].Points > BestScore)
                {
                    NbPlayerWinner = Boucle;
                    BestScore = TabPlayers[Boucle].Points;
                }
            }
            return NbPlayerWinner + 1;
        }

        public static Joueur GetPlayerTab(int PlayerNumber)
        {
            return TabPlayers[PlayerNumber];
        }

        //Getter et Setter utiles pour les tests
        public string GetName()
        {
            return Name;
        }

        public int GetAge()
        {
            return Age;
        }

        public int GetPoint()
        {
            return Points;
        }

        public static int GetNbJoueur()
        {
            return NbJoueurs;
        }

        public bool[] GetToChange()
        {
            return MainToChange;
        }
    }
}
