﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace QwirkleGame
{
    public class Plateau
    {
        private static List<Tuile> GrillePieces = new List<Tuile>();
        private static List<Tuile> PieceToAdd = new List<Tuile>();
        private static int[,] GrilleIndex = new int[20, 20]; //Il faut retirer 1 à l'index, car sinon on n'aurait pas pu distinguer une case vide d'une case contenant la première pièce qui est à l'index 0
        private static int[,] PosToAdd = new int[6, 2];
        private static int NbToAdd = 0;
        private static int NbLn = 20;
        private static int NbCol = 20;

        public static int CountPoint()
        {
            throw new NotImplementedException();
        }

        public static void AddPiece()
        {
            for (int Boucle = 0; Boucle < NbToAdd; Boucle++)
            {
                GrillePieces.Add(PieceToAdd[Boucle]);
                GrilleIndex[PosToAdd[Boucle, 0], PosToAdd[Boucle, 1]] = GrillePieces.Count();
            }
        }

        public static Boolean VerifPlacement(Tuile NouvelleTuile, int PosX, int PosY)
        {
            bool Interdiction = false;
            bool SpecialEmptyTabAutorisation = false;
            //Vérification que la case est vide
            if (GrilleIndex[PosX, PosY] != 0)
            {
                Interdiction = true;
            }
            for (int BouclePieceToAdd = 0; BouclePieceToAdd < NbToAdd; BouclePieceToAdd++)
            {
                if (PosToAdd[BouclePieceToAdd, 0] == PosX && PosToAdd[BouclePieceToAdd, 1] == PosY)
                {
                    Interdiction = true;
                }
            }
            //Vérification qu'on a au moins une case placée autour sauf si c'est la première case
            if (Interdiction == false)
            {
                if (GrilleIndex[PosX + 1, PosY] == 0 && GrilleIndex[PosX - 1, PosY] == 0 && GrilleIndex[PosX, PosY + 1] == 0 && GrilleIndex[PosX, PosY - 1] == 0)
                {
                    Interdiction = true;
                }
                bool Autorisation = false;
                for (int BouclePieceToAdd = 0; BouclePieceToAdd < NbToAdd; BouclePieceToAdd++)
                {
                    if (PosToAdd[BouclePieceToAdd, 0] != PosX - 1 && PosToAdd[BouclePieceToAdd, 1] != PosY) { }
                    else if (PosToAdd[BouclePieceToAdd, 0] != PosX + 1 && PosToAdd[BouclePieceToAdd, 1] != PosY) { }
                    else if (PosToAdd[BouclePieceToAdd, 0] != PosX && PosToAdd[BouclePieceToAdd, 1] != PosY + 1) { }
                    else if (PosToAdd[BouclePieceToAdd, 0] != PosX && PosToAdd[BouclePieceToAdd, 1] != PosY - 1) { }
                    else
                    {
                        Autorisation = true;
                    }
                }
                if (Autorisation == true && Interdiction == true)
                {
                    Interdiction = false;
                }
                if (Interdiction == true)
                {
                    bool EmptyTab = true;
                    for (int BoucleCol = 0; BoucleCol < NbCol; BoucleCol++)
                    {
                        for (int BoucleLn = 0; BoucleLn < NbLn; BoucleLn++)
                        {
                            if (GrilleIndex[BoucleCol, BoucleLn] != 0)
                            {
                                EmptyTab = false;
                            }
                        }
                    }
                    if (EmptyTab == true)
                    {
                        SpecialEmptyTabAutorisation = true; //Autorisation spéciale
                    }
                }
            }
            //Vérification des couleurs et des formes
            if (Interdiction == false)
            {
                ///Vérification des couleurs
                bool ColorInterdiction = false;
                //Vérification de la présence d'une tuile sur l'axe X+
                bool[] Colors = new bool[6]; //Yellow, Blue, Pink, Green, Orange, Red
                bool PresentTuile;
                int CompteurLn = 0;
                do //Boucle pour chaque tuile de la ligne, on va s'assurer que la tuile suivante existe et on va indiquer les couleurs trouvées
                {
                    CompteurLn++;
                    PresentTuile = false;
                    if (GrilleIndex[PosX + 1, PosY] != 0)
                    {
                        PresentTuile = true;
                        if (GrillePieces[GrilleIndex[PosX + CompteurLn, PosY] - 1].GetCouleur() == "Yellow")
                        {
                            Colors[0] = true;
                        }
                        else if (GrillePieces[GrilleIndex[PosX + CompteurLn, PosY] - 1].GetCouleur() == "Blue")
                        {
                            Colors[1] = true;
                        }
                        else if (GrillePieces[GrilleIndex[PosX + CompteurLn, PosY] - 1].GetCouleur() == "Pink")
                        {
                            Colors[2] = true;
                        }
                        else if (GrillePieces[GrilleIndex[PosX + CompteurLn, PosY] - 1].GetCouleur() == "Green")
                        {
                            Colors[3] = true;
                        }
                        else if (GrillePieces[GrilleIndex[PosX + CompteurLn, PosY] - 1].GetCouleur() == "Orange")
                        {
                            Colors[4] = true;
                        }
                        else if (GrillePieces[GrilleIndex[PosX + CompteurLn, PosY] - 1].GetCouleur() == "Red")
                        {
                            Colors[5] = true;
                        }
                    }
                    for (int BouclePieceToAdd = 0; BouclePieceToAdd < NbToAdd; BouclePieceToAdd++)
                    {
                        if (PosToAdd[BouclePieceToAdd, 0] == PosX + CompteurLn && PosToAdd[BouclePieceToAdd, 0] == PosY)
                        {
                            PresentTuile = true;
                            if (GrillePieces[GrilleIndex[PosX + CompteurLn, PosY] - 1].GetCouleur() == "Yellow")
                            {
                                Colors[0] = true;
                            }
                            else if (GrillePieces[GrilleIndex[PosX + CompteurLn, PosY] - 1].GetCouleur() == "Blue")
                            {
                                Colors[1] = true;
                            }
                            else if (GrillePieces[GrilleIndex[PosX + CompteurLn, PosY] - 1].GetCouleur() == "Pink")
                            {
                                Colors[2] = true;
                            }
                            else if (GrillePieces[GrilleIndex[PosX + CompteurLn, PosY] - 1].GetCouleur() == "Green")
                            {
                                Colors[3] = true;
                            }
                            else if (GrillePieces[GrilleIndex[PosX + CompteurLn, PosY] - 1].GetCouleur() == "Orange")
                            {
                                Colors[4] = true;
                            }
                            else if (GrillePieces[GrilleIndex[PosX + CompteurLn, PosY] - 1].GetCouleur() == "Red")
                            {
                                Colors[5] = true;
                            }
                        }
                    }
                } while (PresentTuile == true);
                //Vérification que aucune couleur placée ne correspond à la couleur voulue
                //Yellow, Blue, Pink, Green, Orange, Red
                if (Colors[0] == true && NouvelleTuile.GetCouleur() == "Yellow")
                {
                    ColorInterdiction = true;
                }
                else if (Colors[1] == true && NouvelleTuile.GetCouleur() == "Blue")
                {
                    ColorInterdiction = true;
                }
                else if (Colors[2] == true && NouvelleTuile.GetCouleur() == "Pink")
                {
                    ColorInterdiction = true;
                }
                else if (Colors[3] == true && NouvelleTuile.GetCouleur() == "Green")
                {
                    ColorInterdiction = true;
                }
                else if (Colors[4] == true && NouvelleTuile.GetCouleur() == "Orange")
                {
                    ColorInterdiction = true;
                }
                else if (Colors[5] == true && NouvelleTuile.GetCouleur() == "Red")
                {
                    ColorInterdiction = true;
                }
                //-------------------------------------------------------------------------------------

            }



            throw new NotImplementedException();
        }

        public static void ResetPiecesToAdd()
        {
            PieceToAdd.Clear();
            NbToAdd = 0;
            PosToAdd = new int[6, 2];
        }

        public static void AddPieceToAdd(Tuile Piece, int PosX, int PosY)
        {
            if (NbToAdd < 6 && PosX > 0 && PosY > 0)
            {
                PosToAdd[NbToAdd, 0] = PosX;
                PosToAdd[NbToAdd, 1] = PosY;
                PieceToAdd.Add(Piece);
                NbToAdd++;
            }
        }

        public static int[,] GetCaseInformation()
        {
            return GrilleIndex;
        }

        public static int[,] GetToAddCaseInformation()
        {
            return PosToAdd;
        }

        public static Tuile GetPiecesInformation(int PosX, int PosY)
        {
            return GrillePieces[GrilleIndex[PosX, PosY] - 1];
        }

        public static Tuile GetToAddPiecesInformation(int Index)
        {
            if (PieceToAdd.Count() - 1 >= Index)
            {
                return PieceToAdd[Index];
            }
            else
            {
                return null;
            }
        }

        //Getter et Setter utiles pour les tests

        public static int GetPiecesToAdd()
        {
            return PieceToAdd.Count(); ;
        }

        public static int GetNbPiecesToAdd()
        {
            return NbToAdd;
        }

        public static int[,] GetPosToAdd()
        {
            return PosToAdd;
        }
    }
}
