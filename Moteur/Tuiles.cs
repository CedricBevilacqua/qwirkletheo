﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QwirkleGame
{
    public class Tuile
    {
        //Attributs
        private static List<Tuile> SelectedToChange;
        private static List<Tuile> SelectedToPlace;
        private string Form; //Star, Cross, Lozenge, Flower, Circle, Square
        private string Color; //Yellow, Blue, Pink, Green, Orange, Red

        // Constructeur
        public Tuile(string Form, string Color)
        {
            this.Form = Form;
            this.Color = Color;
        }

        //Méthodes
        public string GetForme()
        {
            return Form;
        }

        public string GetCouleur()
        {
            return Color;
        }

        

        public void SelectToChange(Tuile SelectedTuile)
        {
            throw new NotImplementedException();
        }

        public void ResetSelectionChange()
        {
            throw new NotImplementedException();
        }

        public void DeleteSelectionChange()
        {
            throw new NotImplementedException();
        }

        public List<Tuile> ShowSelectedToChange()
        {
            throw new NotImplementedException();
        }

        public void SelectToPlace(int PosX, int PosY)
        {
            throw new NotImplementedException();
            //Plateau.AddPieceToAdd(this, PosX, PosY);
        }

        public void RemoveSelectedToPlace()
        {
            throw new NotImplementedException();
        }

        public List<Tuile> SeeSelectedToPlace()
        {
            throw new NotImplementedException();
        }
    }
}
